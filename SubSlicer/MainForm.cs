﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SubSlicer
{
    public partial class MainForm : Form
    {
        private PictureBox _PictureBox;
        private List<SubAreaControl> _SubAreaControls;

        public MainForm()
        {
            InitializeComponent();

            Version version = Assembly.GetExecutingAssembly().GetName().Version;
            Text = $"SubTool by Sanii (v{version})";

            _SubAreaControls = new List<SubAreaControl>();
        }

        private void loadImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "PNG Files|*.png;";

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    Bitmap bmp = new Bitmap(openFileDialog.FileName);
                    if (bmp != null)
                    {
                        if (_PictureBox != null)
                        {
                            imgPanel.Controls.Clear();
                            _SubAreaControls.Clear();

                            _PictureBox.Dispose();
                            _PictureBox = null;
                        }

                        _PictureBox = new PictureBox();
                        _PictureBox.Image = bmp;
                        _PictureBox.SizeMode = PictureBoxSizeMode.AutoSize;

                        subPropertiesControl1.SetMaxValues(_PictureBox.Width, _PictureBox.Height);

                        imgPanel.Controls.Add(_PictureBox);
                    }
                }
            }
        }

        private void loadSubsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_PictureBox != null)
            {
                using (FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog())
                {
                    folderBrowserDialog.RootFolder = Environment.SpecialFolder.MyComputer;

                    if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                    {
                        _PictureBox.Controls.Clear();
                        _SubAreaControls.Clear();

                        DirectoryInfo directoryInfo = new DirectoryInfo(folderBrowserDialog.SelectedPath);

                        foreach (FileInfo fileInfo in directoryInfo.GetFiles("*.sub", SearchOption.TopDirectoryOnly))
                        {
                            SubAreaControl subControl = new SubAreaControl();
                            subControl.SubFile = new SubFile(fileInfo);
                            subControl.Tag = fileInfo.Name;
                            subControl.MouseClick += SubControl_MouseClick;

                            _PictureBox.Controls.Add(subControl);
                            _SubAreaControls.Add(subControl);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Please select an image first!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SubControl_MouseClick(object sender, MouseEventArgs e)
        {
            if (sender is SubAreaControl clickedSubControll)
            {
                foreach (SubAreaControl subControl in _SubAreaControls.Where(p => p.Tag != clickedSubControll.Tag))
                {
                    subControl.IsSelected = false;
                }

                if (clickedSubControll.SubFile != null)
                {
                    subPropertiesControl1.LoadInfo(clickedSubControll.SubFile);
                }
            }
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void dragEnabledToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Globals.DragActive = !Globals.DragActive;
            dragEnabledToolStripMenuItem.Checked = Globals.DragActive;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            dragEnabledToolStripMenuItem.Checked = Globals.DragActive;
        }
    }
}
