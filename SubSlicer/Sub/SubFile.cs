﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubSlicer
{
    public class SubFile
    {
        #region Fields

        private string _Name;
        private string _Title;
        private string _Version;
        private string _Image;
        private int _Left;
        private int _Top;
        private int _Right;
        private int _Bottom;

        public event EventHandler OnUpdate;

        #endregion

        #region Constructor

        public SubFile(FileInfo fileInfo)
        {
            LoadSubFile(fileInfo);
        }

        #endregion

        #region Properties

        public string Name { get => _Name; set => _Name = value; }
        public string Title { get => _Title; set => _Title = value; }
        public string Version { get => _Version; set => _Version = value; }
        public string Image { get => _Image; set => _Image = value; }

        public int Left
        {
            get => _Left;
            set
            {
                _Left = value;
                if (OnUpdate != null)
                {
                    OnUpdate(this, new EventArgs());
                }
            }
        }

        public int Top
        {
            get => _Top;
            set
            {
                _Top = value;
                if (OnUpdate != null)
                {
                    OnUpdate(this, new EventArgs());
                }
            }
        }

        public int Right
        {
            get => _Right;
            set
            {
                _Right = value;
                if (OnUpdate != null)
                {
                    OnUpdate(this, new EventArgs());
                }
            }
        }

        public int Bottom
        {
            get => _Bottom;
            set
            {
                _Bottom = value;
                if (OnUpdate != null)
                {
                    OnUpdate(this, new EventArgs());
                }
            }
        }

        public int Width
        {
            get
            {
                return _Right - _Left;
            }
            set
            {
                Right = _Left + value;
            }
        }

        public int Height
        {
            get
            {
                return _Bottom - _Top;
            }
            set
            {
                Bottom = _Top + value;
            }
        }

        #endregion

        #region Methods

        public void LoadSubFile(FileInfo fileInfo)
        {
            if (File.Exists(fileInfo.FullName))
            {
                _Name = fileInfo.Name;

                string[] lines = File.ReadAllLines(fileInfo.FullName);

                foreach (string line in lines)
                {
                    string[] splittedLine = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                    if (splittedLine.Length == 2)
                    {
                        string key = splittedLine[0];
                        string value = splittedLine[1];

                        switch (key)
                        {
                            case "title":
                                _Title = value;
                                break;
                            case "version":
                                _Version = value;
                                break;
                            case "image":
                                _Image = value.Replace("\"", "");
                                break;
                            case "left":
                                _Left = Convert.ToInt32(value);
                                break;
                            case "top":
                                _Top = Convert.ToInt32(value);
                                break;
                            case "right":
                                _Right = Convert.ToInt32(value);
                                break;
                            case "bottom":
                                _Bottom = Convert.ToInt32(value);
                                break;
                        }
                    }
                }
            }
        }

        public void SetLocation(int left, int top)
        {
            _Right = left + Width;
            _Bottom = top + Height;

            _Left = left;
            _Top = top;
        }

        #endregion
    }
}
