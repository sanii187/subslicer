﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SubSlicer
{
    public partial class SubPropertiesControl : UserControl
    {
        private SubFile _SubFile;

        public SubPropertiesControl()
        {
            InitializeComponent();
        }

        public void SetMaxValues(int width, int height)
        {
            leftNumericUpDown.Minimum = 0;
            leftNumericUpDown.Maximum = width;

            rightNumericUpDown.Minimum = 0;
            rightNumericUpDown.Maximum = width;

            topNumericUpDown.Minimum = 0;
            topNumericUpDown.Maximum = height;

            bottomNumericUpDown.Minimum = 0;
            bottomNumericUpDown.Maximum = height;
        }

        public void LoadInfo(SubFile subFile)
        {
            nameTextBox.DataBindings.Clear();
            nameTextBox.DataBindings.Add("Text", subFile, nameof(SubFile.Name), true, DataSourceUpdateMode.OnPropertyChanged);

            titleTextBox.DataBindings.Clear();
            titleTextBox.DataBindings.Add("Text", subFile, nameof(SubFile.Title), true, DataSourceUpdateMode.OnPropertyChanged);

            versionTextBox.DataBindings.Clear();
            versionTextBox.DataBindings.Add("Text", subFile, nameof(SubFile.Version), true, DataSourceUpdateMode.OnPropertyChanged);

            imageTextBox.DataBindings.Clear();
            imageTextBox.DataBindings.Add("Text", subFile, nameof(SubFile.Image), true, DataSourceUpdateMode.OnPropertyChanged);

            leftNumericUpDown.DataBindings.Clear();
            leftNumericUpDown.DataBindings.Add("Value", subFile, nameof(SubFile.Left), true, DataSourceUpdateMode.OnPropertyChanged);

            topNumericUpDown.DataBindings.Clear();
            topNumericUpDown.DataBindings.Add("Value", subFile, nameof(SubFile.Top), true, DataSourceUpdateMode.OnPropertyChanged);

            rightNumericUpDown.DataBindings.Clear();
            rightNumericUpDown.DataBindings.Add("Value", subFile, nameof(SubFile.Right), true, DataSourceUpdateMode.OnPropertyChanged);

            bottomNumericUpDown.DataBindings.Clear();
            bottomNumericUpDown.DataBindings.Add("Value", subFile, nameof(SubFile.Bottom), true, DataSourceUpdateMode.OnPropertyChanged);

            widthNumericUpDown.DataBindings.Clear();
            widthNumericUpDown.DataBindings.Add("Value", subFile, nameof(SubFile.Width), true, DataSourceUpdateMode.OnPropertyChanged);

            heightNumericUpDown.DataBindings.Clear();
            heightNumericUpDown.DataBindings.Add("Value", subFile, nameof(SubFile.Height), true, DataSourceUpdateMode.OnPropertyChanged);

            _SubFile = subFile;
        }
    }
}
