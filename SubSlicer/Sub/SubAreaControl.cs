﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SubSlicer
{
    public partial class SubAreaControl : Panel
    {
        #region Fields

        private bool _IsSelected;
        private SubFile _SubFile;

        private Point _MouseDownLocation;

        #endregion

        #region Constructor

        public SubAreaControl()
        {
            InitializeComponent();
            BorderStyle = BorderStyle.FixedSingle;
            IsSelected = false;
        }

        #endregion

        #region Properties

        public bool IsSelected
        {
            get
            {
                return _IsSelected;
            }
            set
            {
                _IsSelected = value;

                if (_IsSelected)
                {
                    BackColor = Color.FromArgb(220, Color.CadetBlue);
                    Focus();
                }
                else
                {
                    BackColor = Color.FromArgb(150, Color.AliceBlue);
                }
            }
        }

        public SubFile SubFile
        {
            get
            {
                return _SubFile;
            }
            set
            {
                _SubFile = value;

                if (_SubFile != null)
                {
                    Size = new Size(_SubFile.Width, _SubFile.Height);
                    Location = new Point(_SubFile.Left, _SubFile.Top);

                    _SubFile.OnUpdate += _SubFile_OnUpdate;
                }
            }
        }

        private void _SubFile_OnUpdate(object sender, EventArgs e)
        {
            if (sender is SubFile subFile)
            {
                Size = new Size(subFile.Width, subFile.Height);
                Location = new Point(subFile.Left, subFile.Top);
            }
        }

        #endregion

        #region Methods
        public void SelectArea()
        {
            IsSelected = true;
        }

        #endregion

        #region Events

        private void SubAreaControl_MouseClick(object sender, MouseEventArgs e)
        {
            SelectArea();
        }

        private void SubAreaControl_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _MouseDownLocation = e.Location;
            }
        }

        private void SubAreaControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (Globals.DragActive && e.Button == MouseButtons.Left)
            {
                int newLeft = e.X + Left - _MouseDownLocation.X;
                int newTop = e.Y + Top - _MouseDownLocation.Y;

                if (newLeft + Width > Parent.Width)
                {
                    newLeft = Parent.Width - Width;
                }

                if (newLeft < 0)
                {
                    newLeft = 0;
                }

                if (newTop + Height > Parent.Height)
                {
                    newTop = Parent.Height - Height;
                }

                if (newTop < 0)
                {
                    newTop = 0;
                }

                Left = newLeft;
                Top = newTop;

                if (_SubFile != null)
                {
                    _SubFile.SetLocation(Left, Top);
                }
            }
        }

        #endregion
    }
}
